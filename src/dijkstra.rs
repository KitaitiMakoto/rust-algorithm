struct Edge<'a> {
    dest: &'a Node<'a>,
    cost: usize,
}

struct Node<'a> {
    edges: Vec<&'a Edge<'a>>,
    solved: bool,
    label: &'a str,
}

impl Node<'_> {
    fn new<'a>(label: &'a str) -> Node<'a> {
        Node {
            edges: Vec::new(),
            solved: false,
            label,
        }
    }

    fn add_edge_to<'a>(&self, dest: &'a Node, cost: usize) {
        let edge = &Edge {dest, cost};
        self.edges.push(&edge);
    }
}

struct Graph<'a> {
    nodes: Vec<&'a Node<'a>>,
}

pub fn run() -> Result<(), ()> {
    Ok(())
}
