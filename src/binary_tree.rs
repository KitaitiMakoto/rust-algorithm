// https://laysakura.github.io/2019/12/22/rust-DataStructures-Algorithm-BinaryTree/

#[macro_export]
macro_rules! bin_tree {
    ( val: $val:expr, left: $left:expr, right: $right:expr $(,)? ) => {
	BinaryTree::Node {
	    val: $val,
	    left: Box::new($left),
	    right: Box::new($right),
	}
    };
    ( val: $val:expr, right: $right:expr, $(,)? ) => {
	bin_tree! {
	    val: $val,
	    left: bin_tree!(),
	    right: $right,
	}
    };
    ( val: $val:expr, left: $left:expr $(,)? ) => {
	bin_tree! {
	    val: $val,
	    left: $left,
	    right: bin_tree!(),
	}
    };
    ( val: $val:expr $(,)? ) => {
	bin_tree! {
	    val: $val,
	    left: bin_tree!(),
	    right: bin_tree!(),
	}
    };
    () => {
	BinaryTree::Nil
    };
}

#[derive(Debug, PartialEq, Eq)]
pub enum BinaryTree<T> {
    Nil,
    Node {
	val: T,
	left: Box<BinaryTree<T>>,
	right: Box<BinaryTree<T>>,
    }
}

impl<T> BinaryTree<T> {
    pub fn replace(&mut self, to: Self) {
	*self = to;
    }

    pub fn remove(&mut self) {
	self.replace(BinaryTree::Nil);
    }
}

#[test]
fn test_replace() {
    let mut tree1 = bin_tree! {
	val: 5,
	left: bin_tree! {
	    val: 4,
	    left: bin_tree! {
		val: 11,
		left: bin_tree! { val: 7 },
		right: bin_tree! { val: 2 }
	    }
	}
    };
    let tree2 = bin_tree! {
	val: 8,
	left: bin_tree! { val: 13 },
	right: bin_tree! {
	    val: 4,
	    right: bin_tree! { val: 1 },
	}
    };
    let tree3 = bin_tree! {
	val: 5,
	left: bin_tree! {
	    val: 4,
	    left: bin_tree! {
		val: 11,
		left: bin_tree! { val: 7 },
		right: bin_tree! { val: 2 }
	    }
	},
	right: bin_tree! {
	    val: 8,
	    left: bin_tree! { val: 13 },
	    right: bin_tree! {
		val: 4,
		right: bin_tree! { val: 1 },
	    }
	}
    };
    if let BinaryTree::Node { right, .. } = &mut tree1 {
	// right: &mut Box<BinaryTree>
	// *right: mut Box<BinaryTree>
	// **right: mut BinaryTree
	(**right).replace(tree2);
    };
    assert_eq!(&tree1, &tree3);
}

#[test]
fn leetcode_112_path_sum_dfs() {
    use BinaryTree::{Nil, Node};

    pub fn has_path_sum(root: &BinaryTree<i32>, sum: i32) -> bool {
	fn rec(cur_node: &BinaryTree<i32>, cur_sum: i32, sum: i32) -> bool {
	    match cur_node {
		Nil => panic!("cur_nodeがNilの時は呼び出さないでください"),
		Node { val, left, right } => {
		    let cur_sum = cur_sum + val;
		    // left: &Box<BinaryTree>>
		    // *left: Box<BinaryTree>>
		    // **left: BinaryTree
		    // &(**left): &BinaryTree
		    match (&(**left), &(**right)) {
			(Nil, Nil) => cur_sum == sum,
			// (_, Nil) => rec(&(*left), cur_sum, sum), //is jsut left ok? -> yes
			(_, Nil) => rec(left, cur_sum, sum),
			(Nil, _) => rec(&(*right), cur_sum, sum),
			(_, _) => rec(&(*left), cur_sum, sum) || rec(&(*right), cur_sum, sum),
		    }
		}
	    }
	}

	rec(root, 0, sum)
    }

    let root = bin_tree! {
	val: 5,
	left: bin_tree! {
	    val: 4,
	    left: bin_tree! {
		val: 11,
		left: bin_tree! { val: 7 },
		right: bin_tree! { val: 2 },
	    },
	},
	right: bin_tree! {
	    val: 8,
	    left: bin_tree! { val: 13 },
	    right: bin_tree! {
		val: 4,
		right: bin_tree! { val: 1 },
	    },
	}
    };

    assert_eq!(has_path_sum(&root, 22), true);
}

#[test]
fn leetcode_112_path_sum_bfs() {
    use std::collections::VecDeque;
    use BinaryTree::{Nil, Node};

    pub fn has_path_sum(root: &BinaryTree<i32>, sum: i32) -> bool {
	let mut queue = VecDeque::<(&BinaryTree<i32>, i32)>::new();
	queue.push_back((root, 0));

	while let Some((cur_node, cur_sum)) = queue.pop_front() {
	    match cur_node {
		Nil => panic!("Nilをqueueに詰めないでください"),
		Node { val, left, right } => {
		    let cur_sum = cur_sum + val;

		    match (&(**left), &(**right)) {
			(Nil, Nil) => {
			    if cur_sum == sum {
				return true;
			    }
			},
			(_, Nil) => queue.push_back((&(*left), cur_sum)),
			(Nil, _) => queue.push_back((&(*right), cur_sum)),
			(_, _) => {
			    queue.push_back((&(*left), cur_sum));
			    queue.push_back((&(*right), cur_sum));
			},
		    }
		}
	    }
	}

	false
    }
}

#[test]
fn leetcode_814_binary_tree_pruning() {
    use BinaryTree::{Nil, Node};

    pub fn prune_tree(root: &mut Box<BinaryTree<i32>>) {
	// root: &mut Box<BinaryTree>
	// *root: mut Box<BinaryTree>
	// **root: mut BinaryTree
	// &mut **root: &mut BinaryTree
	match &mut **root {
	    Nil => {}
	    Node { val, left, right } => match (&mut **left, &mut **right) {
		(Nil, Nil) => {
		    if *val == 0 {
			root.remove();
		    }
		}
		(_, Nil) => {
		    prune_tree(left);
		    if let Nil = &mut **left {
			root.remove();
		    }
		}
		(Nil, _) => {
		    prune_tree(right);
		    if let Nil = &mut **right {
			if *val == 0 {
			    root.remove();
			}
		    }
		}
		(_, _) => {
		    prune_tree(left);
		    prune_tree(right);
		    if let (Nil, Nil) = (&mut **left, &mut **right) {
			if *val == 0 {
			    root.remove();
			}
		    }
		}
	    }
	}
    }
}

pub fn run() {
    let root1 = BinaryTree::<i32>::Node {
	val: 5,
	left: Box::new(BinaryTree::<i32>::Node {
	    val: 4,
	    left: Box::new(BinaryTree::<i32>::Node {
		val: 11,
		left: Box::new(BinaryTree::Nil),
		right: Box::new(BinaryTree::Nil),
	    }),
	    right: Box::new(BinaryTree::Nil),
	}),
	right: Box::new(BinaryTree::<i32>::Node {
	    val: 8,
	    left: Box::new(BinaryTree::Nil),
	    right: Box::new(BinaryTree::Nil),
	}),
    };
    let root2 = bin_tree! {
	val: 5,
	left: bin_tree! {
	    val: 4,
	    left: bin_tree! { val: 11 }
	},
	right: bin_tree! { val: 8 }
    };
    assert_eq!(root1, root2);
}
